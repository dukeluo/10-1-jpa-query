package com.twuc.webApp.domain;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class RepositoryQueryTest extends JpaTestBase {
    @Autowired
    private ProductLineRepository productLineRepository;

    @Autowired
    private OfficeRepository officeRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ProductRepository productRepository;

    @Test
    void should_find_the_trains_product_line() {
        // TODO
        //
        // 请在相应的 repository 添加恰当的查询方法实现以下功能：
        //
        // 请找到 "Trains" product line。
        // <--start--
        final ProductLine train = productLineRepository.getProductLineByProductLine("Trains").orElseThrow(NoSuchElementException::new);
        // --end-->
        assertEquals(
            "Model trains are a rewarding hobby for enthusiasts of all ages. Whether you're looking for collectible wooden trains, electric streetcars or locomotives, you'll find a number of great choices for any budget within this category. The interactive aspect of trains makes toy trains perfect for young children. The wooden train sets are ideal for children under the age of 5.",
            train.getTextDescription());
    }

    @Test
    void should_find_office_in_tokyo() {
        // TODO
        //
        // 请在相应的 repository 添加恰当的查询方法实现以下功能：
        //
        // 请找到所在城市为 "Tokyo" 的 Office。
        // <--start--
        final List<Office> officesInTokyo = officeRepository.getOfficesByCity("Tokyo").orElseThrow(NoSuchElementException::new);
        // --end-->

        assertEquals(1, officesInTokyo.size());

        final Office office = officesInTokyo.get(0);
        assertEquals("5", office.getOfficeCode());
    }

    @Test
    void should_find_customer_by_country_and_city() {
        // TODO
        //
        // 请在相应的 repository 添加恰当的查询方法实现以下功能：
        //
        // 请找到国家为法国 "France" 城市为南特 "Nantes" 的顾客。
        // <--start--
        final List<Customer> customers = customerRepository.findCustomersByCountryAndCity("France", "Nantes")
                .orElseThrow(NoSuchElementException::new);
        // --end-->

        assertEquals(2, customers.size());
        assertTrue(customers.stream().anyMatch(c -> c.getCustomerNumber() == 103));
        assertTrue(customers.stream().anyMatch(c -> c.getCustomerNumber() == 119));
    }

    @Test
    void should_find_product_by_description() {
        // TODO
        //
        // 请在相应的 repository 添加恰当的查询方法实现以下功能：
        //
        // 请找到 product line 的 Text Description 中包含 "hobby" 字样的 product。
        // <--start--
        final List<Product> products = productRepository.findProductsByProductLineTextDescriptionContains("hobby")
                .orElseThrow(NoSuchElementException::new);
        // --end-->

        assertEquals(3, products.size());
        assertArrayEquals(
            new String[] { "S18_3259", "S32_3207", "S50_1514" },
            products.stream().map(Product::getProductCode).sorted().toArray(String[]::new));
    }

    @Test
    void should_find_product_by_quantity_in_stock() {
        // TODO
        //
        // 请在相应的 repository 添加恰当的查询方法实现以下功能：
        //
        // 请找到 quantity in stock 在 100 到 200 之间的 product。
        // <--start--
        List<Product> products = productRepository.findProductsByQuantityInStockBetween((short)100, (short)200)
                .orElseThrow(NoSuchElementException::new);
        // --end-->

        assertEquals(2, products.size());
        assertArrayEquals(
            new String[] { "S32_1374", "S32_4289" },
            products.stream().map(Product::getProductCode).sorted().toArray(String[]::new));
    }

    @Test
    void should_order_the_result_of_a_derived_query() {
        // TODO
        //
        // 请在相应的 repository 添加恰当的查询方法实现以下功能：
        //
        // 请找到 quantity in stock 在 100 到 200 之间的 product。并按照 Product Code 升序排序。
        // <--start--
        List<Product> products = productRepository.findProductsByQuantityInStockBetweenOrderByProductCode((short)100, (short)200)
                .orElseThrow(NoSuchElementException::new);;
        // --end-->

        assertEquals(2, products.size());
        assertArrayEquals(
            new String[] { "S32_1374", "S32_4289" },
            products.stream().map(Product::getProductCode).toArray(String[]::new));
    }

    @Test
    void should_limit_the_number_of_results() {
        // TODO
        //
        // 请在相应的 repository 添加恰当的查询方法实现以下功能：
        //
        // 请找到 quantity in stock 在 100 到 599 之间的 product，按照 product code 进行升序排序
        // 并返回前三个。
        // <--start--
        List<Product> products = productRepository.findFirst3ProductsByQuantityInStockBetweenOrderByProductCodeAsc((short)100, (short)599)
                .orElseThrow(NoSuchElementException::new);;
        // --end-->

        assertEquals(3, products.size());
        assertArrayEquals(
            new String[] { "S18_2248", "S18_2795", "S32_1374" },
            products.stream().map(Product::getProductCode).toArray(String[]::new));
    }

    @Test
    void should_limit_the_number_of_results_dynamically_by_paging() {
        // TODO
        //
        // 请在相应的 repository 添加恰当的查询方法实现以下功能：
        //
        // 请使用 Page<> 实现上述查询的功能。
        // <--start--
        Page<Product> products = productRepository.findAllByQuantityInStockBetweenOrderByProductCode((short)100, (short)599, PageRequest.of(0, 3));
        // --end-->

        assertEquals(2, products.getTotalPages());
        assertEquals(3, products.getSize());
        assertArrayEquals(
            new String[] { "S18_2248", "S18_2795", "S32_1374" },
            products.stream().map(Product::getProductCode).toArray(String[]::new));
    }
}
