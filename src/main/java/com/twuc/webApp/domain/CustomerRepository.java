package com.twuc.webApp.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--

    Optional<List<Customer>> findCustomersByCountryAndCity(String country, String city);
    // --end-->
}