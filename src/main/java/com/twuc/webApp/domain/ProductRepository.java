package com.twuc.webApp.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, String> {
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    Optional<List<Product>> findProductsByProductLineTextDescriptionContains(String text);
    Optional<List<Product>> findProductsByQuantityInStockBetween(Short lo, Short hi);
    Optional<List<Product>> findProductsByQuantityInStockBetweenOrderByProductCode(Short lo, Short hi);
    Optional<List<Product>> findFirst3ProductsByQuantityInStockBetweenOrderByProductCodeAsc(Short lo, Short hi);
    public Page<Product> findAllByQuantityInStockBetweenOrderByProductCode(Short begin, Short end, Pageable pageable);
    // --end-->
}