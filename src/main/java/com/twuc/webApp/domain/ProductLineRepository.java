package com.twuc.webApp.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProductLineRepository extends JpaRepository<ProductLine, String> {
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    Optional<ProductLine> getProductLineByProductLine(String productLine);
    // --end-->
}
